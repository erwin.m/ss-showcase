package salestock.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import salestock.dao.CategoryDAO;
import salestock.domain.Category;
import salestock.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryDAO categoryDAO;

    public Category save(Category c) {
        return categoryDAO.save(c);
    }

}
