package salestock.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import salestock.dao.ProductDAO;
import salestock.domain.Product;
import salestock.service.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDAO productDAO;

    @Transactional
    public Product save(Product p) {
        return productDAO.save(p);
    }

    @Transactional
    public void update(Product p) {
        productDAO.update(p);
    }

    public Product getByCode(String code) {
        return productDAO.getByCode(code);
    }

}
