package salestock.dao.impl;

import org.springframework.stereotype.Repository;

import salestock.dao.AbstractBaseDAO;
import salestock.dao.CategoryDAO;
import salestock.domain.Category;

@Repository
public class CategoryDAOImpl extends AbstractBaseDAO<Category>
        implements CategoryDAO {

}
