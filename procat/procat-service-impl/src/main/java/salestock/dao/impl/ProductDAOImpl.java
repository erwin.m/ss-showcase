package salestock.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import salestock.dao.AbstractBaseDAO;
import salestock.dao.ProductDAO;
import salestock.domain.Product;

@Repository
public class ProductDAOImpl extends AbstractBaseDAO<Product>
        implements ProductDAO {

    public Product getByCode(String code) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("code", code);
        return (Product) get("from Product p where p.code = :code", param);
    }

}
