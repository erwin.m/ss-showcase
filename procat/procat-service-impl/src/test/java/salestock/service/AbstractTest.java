package salestock.service;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

@ContextConfiguration(locations={"/applicationContext.xml"})
public abstract class AbstractTest extends AbstractTestNGSpringContextTests {

}
