package salestock.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import salestock.domain.Product;

public class ProductTest extends AbstractTest {

    @Autowired
    ProductService productService;

    @Test
    public void testCreate() {
        String code = "code" + System.currentTimeMillis();
        Product productA = new Product(code, new Integer(3), "RED",
                new BigDecimal(100));
        Product saveProduct = productService.save(productA);

        Product realProduct = productService.getByCode(code);
        Assert.assertTrue(saveProduct.equals(realProduct));

        Product fakeProduct = productService.getByCode("unavailable");
        Assert.assertNull(fakeProduct);
    }

}
