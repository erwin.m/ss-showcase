package salestock.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import salestock.domain.Category;

public class CategoryTest extends AbstractTest {

    @Autowired
    CategoryService categoryService;

    @Test
    public void doSave() {
        Category c = new Category("code", "good", null);
        categoryService.save(c);
    }

}
