package salestock.dao;

import salestock.domain.Product;

public interface ProductDAO extends BaseDAO<Product>{

    Product getByCode(String code);

}
