package salestock.dao;

import salestock.domain.Category;

public interface CategoryDAO extends BaseDAO<Category> {

}
