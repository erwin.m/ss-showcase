package salestock.dao;

import javax.persistence.EntityManager;

public interface BaseDAO<T> {

    T save(T entity);

    void update(T entity);

    void remove(T entity);

    EntityManager getEntityManager();

    void setEntityManager(EntityManager em);
}
