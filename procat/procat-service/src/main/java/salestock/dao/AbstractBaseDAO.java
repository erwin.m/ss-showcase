package salestock.dao;

import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public abstract class AbstractBaseDAO<T> implements BaseDAO<T> {

    protected EntityManager em;

    public EntityManager getEntityManager() {
        return em;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager aEm) {
        this.em = aEm;
    }

    public T save(T entity) {
        return getEntityManager().merge(entity);
    }

    public void update(T entity) {
        getEntityManager().persist(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(entity);
    }

    protected Object get(String query, Object... param) {
        Query q = getEntityManager().createQuery(query);
        if (param.length == 1 && param[0] instanceof Map) {
            setParameterMap(q, (Map<String, Object>) param[0]);
        } else {
            setParameter(q, param);
        }
        return get(q);
    }

    protected Object get(Query query) {
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    protected void setParameter(Query q, Object... param) {
        int i = 1;
        for (Object object : param) {
            q.setParameter(i++, object);
        }
    }

    protected void setParameterMap(Query q, Map<String, Object> param) {
        for (String key : param.keySet()) {
            q.setParameter(key, param.get(key));
        }
    }

}
