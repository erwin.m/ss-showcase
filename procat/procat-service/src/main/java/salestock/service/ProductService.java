package salestock.service;

import javax.transaction.Transactional;

import salestock.domain.Product;

@Transactional
public interface ProductService {

    Product save(Product p);

    void update(Product p);

    Product getByCode(String code);
}
