package salestock.service;

import javax.transaction.Transactional;

import salestock.domain.Category;

@Transactional
public interface CategoryService {

    Category save(Category c);

}
