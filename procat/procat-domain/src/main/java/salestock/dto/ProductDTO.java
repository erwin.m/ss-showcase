package salestock.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductDTO implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 8429287895746241845L;

    private String code;
    private Integer size;
    private BigDecimal price;
    private String color;
    
    public String getCode() {
        return code;
    }
    public Integer getSize() {
        return size;
    }
    public BigDecimal getPrice() {
        return price;
    }
    public String getColor() {
        return color;
    }
    public ProductDTO(String code, Integer size, BigDecimal price,
            String color) {
        super();
        this.code = code;
        this.size = size;
        this.price = price;
        this.color = color;
    }

    
}
