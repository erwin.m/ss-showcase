package salestock.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CATEGORYT")
public class Category extends AbstractDomainModel {

    /**
     * 
     */
    private static final long serialVersionUID = 9142281190026155039L;

    private Integer id;
    private String code;
    private String name;
    private Category child;

    public Category() {
    }

    public Category(String code, String name, Category child) {
        this.code = code;
        this.name = name;
        this.child = child;
    }

    public Category getChild() {
        return child;
    }

    public String getCode() {
        return code;
    }

    @GenericGenerator(name = "generator", strategy = "increment")
    @Id
    @GeneratedValue(generator = "generator")
    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setChild(Category child) {
        this.child = child;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

}
