package salestock.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "PRODUCT")
public class Product extends AbstractDomainModel {

    /**
     * 
     */
    private static final long serialVersionUID = -5965440062239811188L;
    private Integer id;
    private Integer size;
    private String color;
    private String code;
    private BigDecimal price;

    public Product() {

    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    @GenericGenerator(name = "generator", strategy = "increment")
    @Id
    @GeneratedValue(generator = "generator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Product(String aCode, Integer aSize, String aColor,
            BigDecimal aPrice) {
        this.size = aSize;
        this.color = aColor;
        this.price = aPrice;
        this.code = aCode;
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return this.getId().equals(((Product) obj).getId())
                || this.getCode().equals(((Product) obj).getCode());
    }
}
