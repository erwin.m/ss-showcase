package salestock.web.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import salestock.domain.Category;
import salestock.domain.Product;
import salestock.service.CategoryService;
import salestock.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    ResponseEntity<Product> put(@RequestBody Product p) {

        Category c = new Category("code", "good", null);
        categoryService.save(c);

        Product saveP = productService.save(p);
        return new ResponseEntity<Product>(saveP, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    void post(@RequestBody Product p) {
        productService.update(p);
    }

    @RequestMapping(value = "/{productCode}", method = RequestMethod.GET)
    ResponseEntity<Product> get(
            @PathVariable("productCode") String productCode) {
        Product realProduct = productService.getByCode(productCode);
        if (realProduct != null) {
            return new ResponseEntity<Product>(realProduct, HttpStatus.OK);
        } else {
            Product productA = new Product("code", new Integer(3), "OK",
                    new BigDecimal(100));
            return new ResponseEntity<Product>(productA, HttpStatus.OK);
        }
    }

}
