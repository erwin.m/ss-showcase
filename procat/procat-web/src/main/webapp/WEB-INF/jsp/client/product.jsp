<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product Manipulation</title>
<script type="text/javascript"
	src="${context}/resources/jquery-ui-1.11.4/jquery.js"></script>
<link href="${context}/resources/jquery-ui-1.11.4/jquery-ui.css"
	rel="stylesheet">
<script src="${context}/resources/jquery-ui-1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
	var context = "${context}";
	$(document).ready(function() {
		$("#loadbutton").button();
		$("#storebutton").button();
		$("#tabs").tabs();

		// clicked button to retrieve product based on code
		$("#loadbutton").click(function() {
			var pcode = $("#loadcode").val();
			$.getJSON(context + "/product/" + pcode, function(data) {
				$("#loadForm [name='size']").val(data.size);
				$("#loadForm [name='price']").val(data.price);
				$("#loadForm [name='color']").val(data.color);
				$("#loadForm [name='code']").val(data.code);
			});
		});
		// clicked button to store/put product based on code
		$("#storebutton").click(function() {
			var data = {"size":"", "color": "", "code" : "", "price" : 0};
			data.code = $("#storeform [name='code']").val();
			data.size = $("#storeform [name='size']").val();
			data.color = $("#storeform [name='color']").val();
			data.price = $("#storeform [name='price']").val();
			$.ajax({
				url : "${context}/product/",
				contentType: "application/json; charset=utf-8",
				type : "PUT",
				dataType: "json",
				data: JSON.stringify(data),
				success : function(result) {
					// void
				}
			});
		});
	});
</script>
</head>
<body>

	<div id="tabs">
		<ul>
			<li><a href="#tabs-1">Load Product</a></li>
			<li><a href="#tabs-2">Store Product</a></li>
			<li><a href="#tabs-3">Update Product</a></li>
		</ul>
		<div id="tabs-1">
			<button id="loadbutton">Load Product</button>
			<input type="text" name="loadcode" id="loadcode" />
			<form action="" method="post" id="loadForm">
				<p>
					<label for="code">Code</label> <input type="text" name="code" />
				</p>
				<p>
					<label for="color">Color</label> <input type="text" name="color" />
				</p>
				<p>
					<label for="size">Size</label> <input type="text" name="size" />
				</p>
				<p>
					<label for="price">Price</label> <input type="text" name="price" />
				</p>

			</form>

		</div>
		<div id="tabs-2">
			<button id="storebutton">Store Product (PUT)</button>
			<form action="" method="post" id="storeform">
				<p>
					<label for="code">Code</label> <input type="text" name="code" />
				</p>
				<p>
					<label for="color">Color</label> <input type="text" name="color" />
				</p>
				<p>
					<label for="size">Size</label> <input type="text" name="size" />
				</p>
				<p>
					<label for="price">Price</label> <input type="text" name="price" />
				</p>

			</form>

		</div>
		<div id="tabs-3">Nam dui erat, auctor a, dignissim quis,
			sollicitudin eu, felis. Pellentesque nisi urna, interdum eget,
			sagittis et, consequat vestibulum, lacus. Mauris porttitor
			ullamcorper augue.</div>
	</div>

</body>
</html>